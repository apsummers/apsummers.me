module.exports = {
  theme: {
    extend: {
      fontFamily: {
        display: ['"Roboto Slab"'],
        body: ['"Fira Sans"'],
        serif: ['"Playfair Display"'],
      },
    },
  },
  variants: {},
  plugins: [],
}
